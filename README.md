## Learning Serverless Cloud Hosting - Part 1

The aim of this project is to understand the basics of serverless cloud based hosting.

## User Stories

As a developer,  
I want to add a simple static html page to a google cloud platform bucket,  
So that I know I can host files via GCP.

As a developer,  
I want to publish my html page on the internet,  
So that others can see my page.

As a developer,  
I want to create a CI pipeline which I can use to update the html document,  
So that I can change the document whenever I want to.  

## Future Build Ideas

- Use a simple webserver (express?).
- Use Kubernetese and Docker.
- Creaate a react app to serve via the Docker image.

[See Part 2 of this Project](https://gitlab.com/jhylins/learning-react-deploy)

## Resources

[GCP Static Website Docs](https://cloud.google.com/storage/docs/hosting-static-website)  
[GCP Static Website Example](https://cloud.google.com/storage/docs/static-website)
[Yogesh Lakhotia Blog](https://medium.com/google-cloud/automatically-deploy-to-google-app-engine-with-gitlab-ci-d1c7237cbe11)
